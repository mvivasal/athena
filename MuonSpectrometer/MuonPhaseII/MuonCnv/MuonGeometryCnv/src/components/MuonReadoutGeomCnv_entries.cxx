/*
   Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "../ReadoutGeomCnvAlg.h"

DECLARE_COMPONENT(MuonGMR4::ReadoutGeomCnvAlg);