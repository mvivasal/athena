# MCTruthSimAlgs
Author Guillaume Unal (gunal@mail.cern.ch)
Converted from packagedoc.h

## Introduction

This package provides a few helper functions for dealing with MC Truth (or in particular for persistent collections saved by truth).  These algorithms are really used for merging two events into one.

## Class Overview

There are two classes in this package:

 - MergeCalibHits : For merging callibration hit collections
 - MergeTrackRecordCollection : For merging collections of track records
